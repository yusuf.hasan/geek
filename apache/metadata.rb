name 'apache'
maintainer 'Yusuf Cochinwala'
maintainer_email 'yusufhc@gmail.com'
license 'Apache 2.0'
description 'Cookbook to setup an HTTP/HTTPS redirector proxy'
version '0.1.0'

depends 'apache2'