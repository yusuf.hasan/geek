include_recipe 'apache2'
include_recipe 'apache2::mod_rewrite'

ruby_block "insert_line" do
  block do
    file = Chef::Util::FileEdit.new("/etc/httpd/conf/httpd.conf")
    file.insert_line_if_no_match("/CustomLog /var/log/httpd/access.log vhost_combined/", "CustomLog /var/log/httpd/access.log vhost_combined")
    file.write_file
  end
end

template "/var/www/html/index.html" do
	source 'index.html.erb'
	mode 0755
	owner "root"
	group "root"
end

template "/etc/httpd/sites-enabled/urlredirect_http_default.conf" do
	source 'urlredirect_http_default.conf.erb'
	mode 0755
	owner "root"
	group "root"
end

template "/etc/httpd/sites-enabled/urlredirect_http_mylocalfuneraldirector.conf" do
	source 'urlredirect_http_mylocalfuneraldirector.conf.erb'
	mode 0755
	owner "root"
	group "root"
end

template "/etc/httpd/sites-enabled/vhost.conf" do
	source 'vhost.conf.erb'
	mode 0755
	owner "root"
	group "root"
end

apache_site "default" do
  enable true
end